# Pikiring Waskitha Hutama
>Team Leader - Developer - Self Motivated - Adabtable

## My Portofolio

 1. ### [Corporate Faceoffice](https://corp.faceoffice.co.id/login)
	
		April 2018 - Present
		Human resource management system include Payroll, Overtime, Leave, Company Management, Employee management, Attendance and many more features.
	
	![faceoffice-payroll](https://i.ibb.co/PNx9zxg/corporate.png)
	
 2. ### [Docobilling E-Statement](http://dev-billing.docoblast.com/login))
		January 2019 - May 2019
		Mail/Statement broadcaster and service 
		
	![e-statement](https://i.ibb.co/zZvNyQ8/Docobilling.png)
 
 3. ### [KTA Notaris (Ditjen AHU)]([https://ahu.go.id/](https://ahu.go.id/))
		November 2017 - January 2018
		Notary member sign card for AHU Director General and integrated with BNI payments

	![ahu.go.id](https://i.ibb.co/R7shcHS/kta-notaris.png)

 4. ### [BNI Smart Tenant](https://bni-smarttenant.com/)
		 September 2017 - November 2017
		 Tenant management for BNI customer on industrial area

	![bni smart tenant](https://i.ibb.co/KqhHfNM/bni-smart-tenant.png)
	
 5. ### Aplikasi Internal
		 August 2017 - September 2017
		 Internal office apps for employee management

 6. ### [Aplikasi Pelaporan & Monitoring Notaris Kemenkumham DIY](http://kanwil-jogja.docotel.net/)
		May 2017 - August 2017
		Reporting / complaints application and monitoring notary activities
		
	![pelaporan hotaris](https://i.ibb.co/qmQHCBf/pelaporan-notaris.png)
	
 7. ### [Website Kemenkumham DIY](http://portalyogya.docotel.net/)
		February 2017 - August 2017
		Company Profile & News

	![portal](https://i.ibb.co/KxG7RQ5/portal-ahu-jogja.png)
	
 8. ### [SIMRS Marzoeki Mahdi Bogor](https://sim.rsmmbogor.com/)
		September 2016 - February 2017
		Hospital management system for RSMM Bogor. (Module medical records, surgery rooms, services)
 
 9. ### [Geschool Learning & Parents ](https://play.google.com/store/apps/details?id=net.geschool.mobile.learning)
		March 2015 - October 2015
		Mobile learning apps for student and teacher 

## I'm available for new opportunities
#### Please contact me at [LinkedIn](https://www.linkedin.com/in/pikiring-waskitha-hutama-518816104/) or email to pikiring.waskitha@gmail.com
